package webflow

// Service interface to dictate methods service should implement
type Service interface {
	Test() error
}

type service struct {
	apiKey string
}

// NewService creates new instance of auth service
func NewService(apiKey string) Service {
	return &service{apiKey}
}

// GetUserToken authorises a user and returns a the response from the auth challenge
// Or returns an error
func (s *service) Test() (err error) {
	return nil
}
