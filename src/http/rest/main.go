package rest

import (
	"net/http"

	webflow "bitbucket.org/bananabread/property-expert-server.git/pkg/webflow"

	echo "github.com/labstack/echo/v4"
)

// Handler handles all requests received and runs appropriate func
func Handler(webflow webflow.Service) *echo.Echo {
	r := echo.New()
	// r.Validator = validator.CreateValidator()

	// firmware
	r.GET("/test", getTest())

	// Permission check
	r.GET("/_permission", func(c echo.Context) error {
		return c.String(http.StatusOK, "OK")
	})

	return r
}

// loadIndex returns a html blob pointing to the static bundles index.html.
func getTest() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.String(http.StatusOK, "Test")
	}
}
