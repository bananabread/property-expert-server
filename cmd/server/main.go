package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/labstack/echo/v4/middleware"

	rest "bitbucket.org/bananabread/property-expert-server.git/pkg/http/rest"
	webflow "bitbucket.org/bananabread/property-expert-server.git/pkg/webflow"
)

var (
	serviceName = "property-expert-server"
	port        string
	err         error
)

func main() {
	// Configure server
	srv, err := configureServer()
	if err != nil {
		log.Fatal(err)
	}

	errChan := make(chan error)
	go func() {
		errChan <- srv.ListenAndServe()
	}()

	log.Printf(fmt.Sprintf("firmware-uploader tool listening on %s", port))
	log.Fatal(<-errChan)
}

func configureServer() (s *http.Server, err error) {
	// Init services
	webflow := webflow.NewService()

	// Init handler
	handler := rest.Handler(webflow)

	handler.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{},
		AllowMethods:     []string{http.MethodGet},
		AllowCredentials: true,
	}))

	s = &http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      handler,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	return s, nil
}
